# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015.
# SPDX-FileCopyrightText: 2023 Adrián Chaves (Gallaecio)
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2023-11-23 17:35+0100\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Adrian Chaves (Gallaecio)"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "adrian@chaves.io"

#: calendar/eventdatadecorator.cpp:51
#, kde-format
msgctxt "Agenda listview section title"
msgid "Holidays"
msgstr "Vacacións"

#: calendar/eventdatadecorator.cpp:53
#, kde-format
msgctxt "Agenda listview section title"
msgid "Events"
msgstr "Eventos"

#: calendar/eventdatadecorator.cpp:55
#, kde-format
msgctxt "Agenda listview section title"
msgid "Todo"
msgstr "Tarefas"

#: calendar/eventdatadecorator.cpp:57
#, kde-format
msgctxt "Means 'Other calendar items'"
msgid "Other"
msgstr "Outros"

#: calendar/qml/DayDelegate.qml:43
#, kde-format
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1 evento"
msgstr[1] "%1 eventos"

#: calendar/qml/DayDelegate.qml:43
#, kde-format
msgid "No events"
msgstr "Ningún evento"

#: calendar/qml/MonthViewHeader.qml:67
#, kde-format
msgctxt "Format: month year"
msgid "%1 %2"
msgstr "%1 de %2"

#: calendar/qml/MonthViewHeader.qml:114
#, kde-format
msgid "Days"
msgstr "Días"

#: calendar/qml/MonthViewHeader.qml:120
#, kde-format
msgid "Months"
msgstr "Meses"

#: calendar/qml/MonthViewHeader.qml:126
#, kde-format
msgid "Years"
msgstr "Anos"

#: calendar/qml/MonthViewHeader.qml:164
#, kde-format
msgid "Previous Month"
msgstr "Mes anterior"

#: calendar/qml/MonthViewHeader.qml:166
#, kde-format
msgid "Previous Year"
msgstr "Ano anterior"

#: calendar/qml/MonthViewHeader.qml:168
#, kde-format
msgid "Previous Decade"
msgstr "Década anterior"

#: calendar/qml/MonthViewHeader.qml:185
#, kde-format
msgctxt "Reset calendar to today"
msgid "Today"
msgstr "Hoxe"

#: calendar/qml/MonthViewHeader.qml:186
#, kde-format
msgid "Reset calendar to today"
msgstr "Devolver o calendario a hoxe"

#: calendar/qml/MonthViewHeader.qml:197
#, kde-format
msgid "Next Month"
msgstr "Seguinte mes"

#: calendar/qml/MonthViewHeader.qml:199
#, kde-format
msgid "Next Year"
msgstr "Seguinte ano"

#: calendar/qml/MonthViewHeader.qml:201
#, kde-format
msgid "Next Decade"
msgstr "Seguinte década"

#: calendar/qml/MonthViewHeader.qml:257
#, kde-format
msgid "Keep Open"
msgstr "Manter aberto"

#: containmentlayoutmanager/qml/BasicAppletContainer.qml:270
#, kde-format
msgid "Configure…"
msgstr "Configurar…"

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:10
#, kde-format
msgid "Screen lock enabled"
msgstr "Activouse o bloqueo da pantalla"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:11
#, kde-format
msgid "Sets whether the screen will be locked after the specified time."
msgstr "Define se se bloqueará ou non a pantalla pasado o tempo indicado."

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:16
#, kde-format
msgid "Screen saver timeout"
msgstr "Tempo de agarda do protector de pantalla"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:17
#, kde-format
msgid "Sets the minutes after which the screen is locked."
msgstr ""
"Define o número de minutos que deben pasar para que se bloquee a pantalla."

#: sessionsprivate/sessionsmodel.cpp:235 sessionsprivate/sessionsmodel.cpp:239
#, kde-format
msgid "New Session"
msgstr "Nova sesión"

#: shellprivate/widgetexplorer/kcategorizeditemsviewmodels.cpp:64
#, kde-format
msgid "Filters"
msgstr "Filtros"

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:148
#, kde-format
msgid ""
"This Widget was written for an unknown older version of Plasma and is not "
"compatible with Plasma %1. Please contact the widget's author for an updated "
"version."
msgstr ""
"O trebello escribiuse para unha versión anterior e descoñecida de Plasma e "
"non é compatíbel con Plasma %1. Solicite a quen fixo o trebello unha versión "
"actualizada."

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:152
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please contact the widget's author for an updated version."
msgstr ""
"O trebello escribiuse para Plasma %1 e non é compatíbel con Plasma %2. "
"Solicite a quen fixo o trebello unha versión actualizada."

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:156
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please update Plasma in order to use the widget."
msgstr ""
"O trebello escribiuse para Plasma %1 e non é compatíbel con Plasma %2. "
"Actualice Plasma para usar o trebello."

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:161
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with the latest "
"version of Plasma. Please update Plasma in order to use the widget."
msgstr ""
"O trebello escribiuse para Plasma %1 e non é compatíbel coa última versión "
"de Plasma. Actualice Plasma para usar o trebello."

#: shellprivate/widgetexplorer/widgetexplorer.cpp:111
msgctxt "applet category"
msgid "Accessibility"
msgstr "Accesibilidade"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:112
msgctxt "applet category"
msgid "Application Launchers"
msgstr "Iniciadores de aplicacións"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:113
msgctxt "applet category"
msgid "Astronomy"
msgstr "Astronomía"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:114
msgctxt "applet category"
msgid "Date and Time"
msgstr "Data e hora"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:115
msgctxt "applet category"
msgid "Development Tools"
msgstr "Ferramentas de desenvolvemento"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:116
msgctxt "applet category"
msgid "Education"
msgstr "Educación"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:117
msgctxt "applet category"
msgid "Environment and Weather"
msgstr "Medio ambiente e clima"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:118
msgctxt "applet category"
msgid "Examples"
msgstr "Exemplos"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:119
msgctxt "applet category"
msgid "File System"
msgstr "Sistema de ficheiros"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:120
msgctxt "applet category"
msgid "Fun and Games"
msgstr "Lecer e xogos"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:121
msgctxt "applet category"
msgid "Graphics"
msgstr "Gráficos"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:122
msgctxt "applet category"
msgid "Language"
msgstr "Idioma"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:123
msgctxt "applet category"
msgid "Mapping"
msgstr "Mapas"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:124
msgctxt "applet category"
msgid "Miscellaneous"
msgstr "Diversas"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:125
msgctxt "applet category"
msgid "Multimedia"
msgstr "Multimedia"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:126
msgctxt "applet category"
msgid "Online Services"
msgstr "Servizos en Internet"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:127
msgctxt "applet category"
msgid "Productivity"
msgstr "Produtividade"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:128
msgctxt "applet category"
msgid "System Information"
msgstr "Información do sistema"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:129
msgctxt "applet category"
msgid "Utilities"
msgstr "Utilidades"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:130
msgctxt "applet category"
msgid "Windows and Tasks"
msgstr "Xanelas e tarefas"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:131
msgctxt "applet category"
msgid "Clipboard"
msgstr "Portapapeis"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:132
msgctxt "applet category"
msgid "Tasks"
msgstr "Tarefas"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:148
#, kde-format
msgid "All Widgets"
msgstr "Todos os trebellos"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:152
#, kde-format
msgid "Running"
msgstr "En execución"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:158
#, kde-format
msgctxt ""
"@item:inmenu used in the widget filter. Filter widgets that can be un-"
"installed from the system, which are usually installed by the user to a "
"local place."
msgid "Uninstallable"
msgstr "Desinstalábel"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:162
#, kde-format
msgid "Categories:"
msgstr "Categorías:"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:232
#, kde-format
msgid "Download New Plasma Widgets"
msgstr "Descargar novos trebellos de Plasma"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:241
#, kde-format
msgid "Install Widget From Local File…"
msgstr "Instalar o trebello dun ficheiro local…"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:503
#, kde-format
msgid "Select Plasmoid File"
msgstr "Seleccione un ficheiro de trebello"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:517
#, kde-format
msgid "Installing the package %1 failed."
msgstr "Fallou a instalación do paquete %1."

#: shellprivate/widgetexplorer/widgetexplorer.cpp:517
#, kde-format
msgid "Installation Failure"
msgstr "Fallo de instalación"

#~ msgid "&Execute"
#~ msgstr "&Executar"

#~ msgctxt "Toolbar Button to switch to Plasma Scripting Mode"
#~ msgid "Plasma"
#~ msgstr "Plasma"

#~ msgctxt "Toolbar Button to switch to KWin Scripting Mode"
#~ msgid "KWin"
#~ msgstr "KWin"

#~ msgid "Templates"
#~ msgstr "Modelos"

#~ msgid "Desktop Shell Scripting Console"
#~ msgstr "Consola de scripting da shell do escritorio"

#~ msgid "Editor"
#~ msgstr "Editor"

#~ msgid "Load"
#~ msgstr "Cargar"

#~ msgid "Use"
#~ msgstr "Usar"

#~ msgid "Output"
#~ msgstr "Saída"

#~ msgid "Unable to load script file <b>%1</b>"
#~ msgstr "Non foi posíbel cargar o ficheiro de script <b>%1</b>"

#~ msgid "Open Script File"
#~ msgstr "Abrir un ficheiro de script"

#~ msgid "Save Script File"
#~ msgstr "Gardar o ficheiro de script"

#~ msgid "Executing script at %1"
#~ msgstr "Estase a executar o script en %1"

#~ msgid "Runtime: %1ms"
#~ msgstr "Duración da execución: %1ms"

#~ msgid "Download Wallpaper Plugins"
#~ msgstr "Descargar complementos de fondo de escritorio"

#~ msgid "Containments"
#~ msgstr "Contedores"
