# translation of plasma_applet_battery.po to Icelandic
# Copyright (C) 2008 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sveinn í Felli <sveinki@nett.is>, 2008, 2009, 2010, 2011.
# SPDX-FileCopyrightText: 2022, 2023, 2024 Guðmundur Erlingsson <gudmundure@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_battery\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-12 00:39+0000\n"
"PO-Revision-Date: 2024-02-19 18:33+0000\n"
"Last-Translator: Guðmundur Erlingsson <gudmundure@gmail.com>\n"
"Language-Team: Icelandic <kde-i18n-doc@kde.org>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.08.3\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"\n"
"\n"
"\n"
"\n"

#: package/contents/ui/BatteryItem.qml:107
#, kde-format
msgctxt "Placeholder is battery percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:171
#, kde-format
msgid ""
"This battery's health is at only %1% and it should be replaced. Contact the "
"manufacturer."
msgstr ""
"Þessi rafhlaða er einungis með %1% getu og þarf að skipta henni út. Hafðu "
"samband við framleiðandann."

#: package/contents/ui/BatteryItem.qml:191
#, kde-format
msgid "Time To Full:"
msgstr "Tími að fullri hleðslu:"

#: package/contents/ui/BatteryItem.qml:192
#, kde-format
msgid "Remaining Time:"
msgstr "Tími sem eftir er:"

#: package/contents/ui/BatteryItem.qml:197
#, kde-format
msgctxt "@info"
msgid "Estimating…"
msgstr "Áætla..."

#: package/contents/ui/BatteryItem.qml:209
#, kde-format
msgid "Battery Health:"
msgstr "Geta rafhlöðu:"

#: package/contents/ui/BatteryItem.qml:215
#, kde-format
msgctxt "Placeholder is battery health percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:229
#, kde-format
msgid "Battery is configured to charge up to approximately %1%."
msgstr "Rafhlaðan er grunnstillt þannig að hún hlaðist upp í u.þ.b. %1%."

#: package/contents/ui/CompactRepresentation.qml:105
#, kde-format
msgctxt "battery percentage below battery icon"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/logic.js:23 package/contents/ui/logic.js:29
#: package/contents/ui/main.qml:142
#, kde-format
msgid "Fully Charged"
msgstr "Fullhlaðin"

#: package/contents/ui/logic.js:28
#, kde-format
msgid "Discharging"
msgstr "Afhleðst"

#: package/contents/ui/logic.js:30
#, kde-format
msgid "Charging"
msgstr "Í hleðslu"

#: package/contents/ui/logic.js:32
#, kde-format
msgid "Not Charging"
msgstr "Ekki í hleðslu"

#: package/contents/ui/logic.js:35
#, kde-format
msgctxt "Battery is currently not present in the bay"
msgid "Not present"
msgstr "Ekki til staðar"

#: package/contents/ui/main.qml:117
#, kde-format
msgid "Power and Battery"
msgstr "Orka og rafhlaða"

#: package/contents/ui/main.qml:117 package/contents/ui/main.qml:286
#, kde-format
msgid "Power Management"
msgstr "Orkustjórnun"

#: package/contents/ui/main.qml:149
#, kde-format
msgid "Battery at %1%, not Charging"
msgstr "Rafhlaða í %1%, ekki að hlaðast"

#: package/contents/ui/main.qml:151
#, kde-format
msgid "Battery at %1%, plugged in but still discharging"
msgstr "Battery í %1%, í sambandi en samt að afhlaðast"

#: package/contents/ui/main.qml:153
#, kde-format
msgid "Battery at %1%, Charging"
msgstr "Rafhlaða í %1%, í hleðslu"

#: package/contents/ui/main.qml:156
#, kde-format
msgid "Battery at %1%"
msgstr "Rafhlaða í %1%"

#: package/contents/ui/main.qml:164
#, kde-format
msgid "The power supply is not powerful enough to charge the battery"
msgstr "Aflgjafinn er ekki nógu öflugur til að hlaða rafhlöðuna"

#: package/contents/ui/main.qml:168
#, kde-format
msgid "No Batteries Available"
msgstr "Engar rafhlöður tiltækar"

#: package/contents/ui/main.qml:174
#, kde-format
msgctxt "time until fully charged - HH:MM"
msgid "%1 until fully charged"
msgstr "%1 þar til fullri hleðslu er náð"

#: package/contents/ui/main.qml:176
#, kde-format
msgctxt "remaining time left of battery usage - HH:MM"
msgid "%1 remaining"
msgstr "%1 eftir"

#: package/contents/ui/main.qml:179
#, kde-format
msgid "Not charging"
msgstr "Ekki í hleðslu"

#: package/contents/ui/main.qml:183
#, kde-format
msgid "Automatic sleep and screen locking are disabled"
msgstr "Sjálfvirk svæfing og skjálæsing eru óvirk"

#: package/contents/ui/main.qml:188
#, kde-format
msgid "An application has requested activating Performance mode"
msgid_plural "%1 applications have requested activating Performance mode"
msgstr[0] "Forrit hefur beðið um að virkja afkastasnið"
msgstr[1] "%1 forrit hafa beðið um að virkja afkastasnið"

#: package/contents/ui/main.qml:192
#, kde-format
msgid "System is in Performance mode"
msgstr "Kerfið er í afkastasniði"

#: package/contents/ui/main.qml:196
#, kde-format
msgid "An application has requested activating Power Save mode"
msgid_plural "%1 applications have requested activating Power Save mode"
msgstr[0] "Forrit hefur beðið um að virkja orkusparnaðarsnið"
msgstr[1] "%1 forrit hafa beðið um að virkja orkusparnaðarsnið"

#: package/contents/ui/main.qml:200
#, kde-format
msgid "System is in Power Save mode"
msgstr "Kerfið er í orkusparnaðarsniði"

#: package/contents/ui/main.qml:260
#, kde-format
msgid "The battery applet has enabled system-wide inhibition"
msgstr "Rafhlöðusmáforritið hefur virkjað takmarkanir fyrir allt kerfið"

#: package/contents/ui/main.qml:299
#, kde-format
msgid "Failed to activate %1 mode"
msgstr "Mistókst að virkja sniðið %1"

#: package/contents/ui/main.qml:311
#, kde-format
msgid "&Show Energy Information…"
msgstr "&Sýna orkuupplýsingar…"

#: package/contents/ui/main.qml:317
#, kde-format
msgid "Show Battery Percentage on Icon When Not Fully Charged"
msgstr ""
"Sýna hleðsluprósentu rafhlöðu á tákni þegar hún er ekki með fulla hleðslu"

#: package/contents/ui/main.qml:330
#, kde-format
msgid "&Configure Power Management…"
msgstr "&Grunnstilla orkustjórnun…"

#: package/contents/ui/PowerManagementItem.qml:41
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid "Manually block sleep and screen locking"
msgstr "Loka handvirkt á svæfingu og skjálæsingu"

#: package/contents/ui/PowerManagementItem.qml:76
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid ""
"Your laptop is configured not to sleep when closing the lid while an "
"external monitor is connected."
msgstr ""
"Fartölvan þín er stillt þannig að hún sofni ekki þegar henni er lokað á "
"meðan ytri skjár er tengdur."

#: package/contents/ui/PowerManagementItem.qml:87
#, kde-format
msgid "%1 application is currently blocking sleep and screen locking:"
msgid_plural "%1 applications are currently blocking sleep and screen locking:"
msgstr[0] "%1 forrit kemur í veg fyrir svæfingu og skjálæsingu:"
msgstr[1] "%1 forrit koma í veg fyrir svæfingu og skjálæsingu:"

#: package/contents/ui/PowerManagementItem.qml:107
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (%2)"
msgstr "%1 kemur í veg fyrir svæfingu og skjálæsingu (%2)"

#: package/contents/ui/PowerManagementItem.qml:109
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (unknown reason)"
msgstr "%1 kemur í veg fyrir svæfingu og skjálæsingu (óþekkt ástæða)"

#: package/contents/ui/PowerManagementItem.qml:111
#, kde-format
msgid "An application is currently blocking sleep and screen locking (%1)"
msgstr "Forrit kemur í veg fyrir svæfingu og skjálæsingu (%1)"

#: package/contents/ui/PowerManagementItem.qml:113
#, kde-format
msgid ""
"An application is currently blocking sleep and screen locking (unknown "
"reason)"
msgstr "Forrit kemur í veg fyrir svæfingu og skjálæsingu (óþekkt ástæða)"

#: package/contents/ui/PowerManagementItem.qml:117
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerManagementItem.qml:119
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: unknown reason"
msgstr "%1: óþekkt ástæða"

#: package/contents/ui/PowerManagementItem.qml:121
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: %1"
msgstr "Óþekkt forrit: %1"

#: package/contents/ui/PowerManagementItem.qml:123
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: unknown reason"
msgstr "Óþekkt forrit: óþekkt ástæða"

#: package/contents/ui/PowerProfileItem.qml:37
#, kde-format
msgid "Power Save"
msgstr "Orkusparnaður"

#: package/contents/ui/PowerProfileItem.qml:41
#, kde-format
msgid "Balanced"
msgstr "Jafnvægi"

#: package/contents/ui/PowerProfileItem.qml:45
#, kde-format
msgid "Performance"
msgstr "Afköst"

#: package/contents/ui/PowerProfileItem.qml:62
#, kde-format
msgid "Power Profile"
msgstr "Orkuprófíll"

#: package/contents/ui/PowerProfileItem.qml:97
#, kde-format
msgctxt "Power profile"
msgid "Not available"
msgstr "Ekki tiltækt"

#: package/contents/ui/PowerProfileItem.qml:192
#, kde-format
msgid ""
"Performance mode has been disabled to reduce heat generation because the "
"computer has detected that it may be sitting on your lap."
msgstr ""
"Afkastasnið var gert óvirkt til að draga úr hitamyndun þar sem tölvan "
"greindi að hún situr hugsanlega í kjöltu þinni."

#: package/contents/ui/PowerProfileItem.qml:194
#, kde-format
msgid ""
"Performance mode is unavailable because the computer is running too hot."
msgstr "Afkastasnið er ekki tiltækt þar sem tölvan er orðin of heit."

#: package/contents/ui/PowerProfileItem.qml:196
#, kde-format
msgid "Performance mode is unavailable."
msgstr "Afkastasnið er ekki tiltækt."

#: package/contents/ui/PowerProfileItem.qml:209
#, kde-format
msgid ""
"Performance may be lowered to reduce heat generation because the computer "
"has detected that it may be sitting on your lap."
msgstr ""
"Mögulega er dregið úr afköstum til að draga úr hitamyndun þar sem tölvan "
"greindi að hún situr hugsanlega í kjöltu þinni."

#: package/contents/ui/PowerProfileItem.qml:211
#, kde-format
msgid "Performance may be reduced because the computer is running too hot."
msgstr "Mögulega er dregið úr afköstum þar sem tölvan er orðin of heit."

#: package/contents/ui/PowerProfileItem.qml:213
#, kde-format
msgid "Performance may be reduced."
msgstr "Mögulega er dregið úr afköstum."

#: package/contents/ui/PowerProfileItem.qml:224
#, kde-format
msgid "One application has requested activating %2:"
msgid_plural "%1 applications have requested activating %2:"
msgstr[0] "Eitt forrit hefur beðið um að %2 sé virkjað:"
msgstr[1] "%1 forrit hafa beðið um að %2 sé virkjað:"

#: package/contents/ui/PowerProfileItem.qml:242
#, kde-format
msgctxt ""
"%1 is the name of the application, %2 is the reason provided by it for "
"activating performance mode"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerProfileItem.qml:261
#, kde-kuit-format
msgid ""
"Power profiles may be supported on your device.<nl/>Try installing the "
"<command>power-profiles-daemon</command> package using your distribution's "
"package manager and restarting the system."
msgstr ""
"Orkuprófílar eru hugsanleg studdir í tækinu þínu.<nl/>Prófaðu að setja "
"upp<command>power-profiles-daemon</command>-pakkann með pakkastjóra "
"stýrikerfisútgáfunnar þinnar og endurræstu tölvuna."

#~ msgctxt "Placeholder is brightness percentage"
#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Battery and Brightness"
#~ msgstr "Rafhlaða og skjábirta"

#~ msgid "Brightness"
#~ msgstr "Birtustig"

#~ msgid "Battery"
#~ msgstr "Rafhlaða"

#~ msgid "Scroll to adjust screen brightness"
#~ msgstr "Skrunaðu til að stilla birtustig skjásins"

#~ msgid "Display Brightness"
#~ msgstr "Skjábirta"

#~ msgid "Keyboard Brightness"
#~ msgstr "Lyklaborðsbirta"

#~ msgid "General"
#~ msgstr "Almennt"

#, fuzzy
#~| msgid "%1% (charging)"
#~ msgid "%1% Charging"
#~ msgstr "%1% (að hlaðast)"

#, fuzzy
#~| msgctxt "tooltip"
#~| msgid "Plugged in"
#~ msgid "%1% Plugged in"
#~ msgstr "Tengd"

#, fuzzy
#~| msgctxt "overlay on the battery, needs to be really tiny"
#~| msgid "%1%"
#~ msgctxt "Placeholder is battery's capacity degradation"
#~ msgid "%1%"
#~ msgstr "%1%"

#, fuzzy
#~| msgctxt "overlay on the battery, needs to be really tiny"
#~| msgid "%1%"
#~ msgctxt "Placeholder is battery capacity"
#~ msgid "%1%"
#~ msgstr "%1%"

#, fuzzy
#~| msgctxt "overlay on the battery, needs to be really tiny"
#~| msgid "%1%"
#~ msgctxt "Placeholder is battery name"
#~ msgid "%1:"
#~ msgstr "%1%"

#, fuzzy
#~| msgid "AC Adapter:"
#~ msgid "AC Adapter"
#~ msgstr "AC hleðslutæki:"

#, fuzzy
#~| msgctxt "tooltip"
#~| msgid "Plugged in"
#~ msgid "Plugged In"
#~ msgstr "Tengd"

#, fuzzy
#~| msgctxt "tooltip"
#~| msgid "Not plugged in"
#~ msgid "Not Plugged In"
#~ msgstr "Ekki í sambandi"

#, fuzzy
#~| msgctxt "Label for remaining time"
#~| msgid "Time Remaining:"
#~ msgid "Time remaining until full: %1"
#~ msgstr "Tími eftir:"

#, fuzzy
#~| msgctxt "Label for remaining time"
#~| msgid "Time Remaining:"
#~ msgid "Time remaining until empty: %1"
#~ msgstr "Tími eftir:"

#, fuzzy
#~| msgid "%1% (charged)"
#~ msgid "%1% (charged)"
#~ msgstr "%1% (að hlaðast)"

#, fuzzy
#~| msgid "AC Adapter:"
#~ msgctxt "tooltip"
#~ msgid "AC Adapter:"
#~ msgstr "AC hleðslutæki:"

#, fuzzy
#~| msgid "<b>Plugged in</b>"
#~ msgctxt "tooltip"
#~ msgid "<b>Plugged in</b>"
#~ msgstr "<b>Í sambandi</b>"

#, fuzzy
#~| msgid "<b>Not plugged in</b>"
#~ msgctxt "tooltip"
#~ msgid "<b>Not plugged in</b>"
#~ msgstr "<b>Ekki í sambandi</b>"

#~ msgctxt "overlay on the battery, needs to be really tiny"
#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Configure Battery Monitor"
#~ msgstr "Stilla rafhlöðumæli"

#~ msgctxt "Suspend the computer to RAM; translation should be short"
#~ msgid "Sleep"
#~ msgstr "Svæfa"

#~ msgctxt "Suspend the computer to disk; translation should be short"
#~ msgid "Hibernate"
#~ msgstr "Leggja í dvala"

#~ msgctxt "Battery is not plugged in"
#~ msgid "<b>Not present</b>"
#~ msgstr "<b>Ekki til staðar</b>"

#~ msgid "Show the state for &each battery present"
#~ msgstr "Sýna &stöðu hleðslu á hverri tengdri rafhlöðu"

#~ msgid "<b>Battery:</b>"
#~ msgstr "<b>Rafhlaða:</b> "

#~ msgctxt "tooltip"
#~ msgid "<b>AC Adapter:</b>"
#~ msgstr "<b>AC hleðslutæki:</b>"

#~ msgctxt "tooltip"
#~ msgid "Not plugged in"
#~ msgstr "Ekki í sambandi"

#, fuzzy
#~| msgid "Show the state for &each battery present"
#~ msgid "Show remaining time for the battery"
#~ msgstr "Sýna &stöðu hleðslu á hverri tengdri rafhlöðu"

#~ msgctxt "tooltip: placeholder is the battery ID"
#~ msgid "<b>Battery %1:</b>"
#~ msgstr "<b>Rafhlaða %1:</b>"
