# translation of plasmashell.pot to esperanto
# Copyright (C) 2014 Free Software Foundation, Inc.
# This file is distributed under the same license as the plasma-workspace package.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
# Minuskloj: ĉ ĝ ĵ ĥ ŝ ŭ   Majuskloj: Ĉ Ĝ Ĵ Ĥ Ŝ Ŭ
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-13 00:39+0000\n"
"PO-Revision-Date: 2024-02-02 22:38+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Oliver Kellogg"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "okellogg@users.sourceforge.net"

#: currentcontainmentactionsmodel.cpp:206
#, kde-format
msgid "Configure Mouse Actions Plugin"
msgstr "Kromprogramo por Agordi Musajn Agojn"

#: desktopview.cpp:211
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Dev"
msgstr "KDE Plasma 6.0 Dev"

#: desktopview.cpp:214
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Alpha"
msgstr "KDE Plasma 6.0 Alpha"

#: desktopview.cpp:217
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Beta 1"
msgstr "KDE Plasma 6.0 Beta 1"

#: desktopview.cpp:220
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Beta 2"
msgstr "KDE Plasma 6.0 Beta 2"

#: desktopview.cpp:223
#, kde-format
msgctxt "@label %1 is the Plasma version, RC meaning Release Candidate"
msgid "KDE Plasma 6.0 RC1"
msgstr "KDE Plasma 6.0 RC1"

#: desktopview.cpp:226
#, kde-format
msgctxt "@label %1 is the Plasma version, RC meaning Release Candidate"
msgid "KDE Plasma 6.0 RC2"
msgstr "KDE Plasma 6.0 RC2"

#: desktopview.cpp:252
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Dev"
msgstr "Plasma %1"

#: desktopview.cpp:256
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Beta"
msgstr "KDE Plasma %1 Beta"

#: desktopview.cpp:259
#, kde-format
msgctxt "@label %1 is the Plasma version, %2 is the beta release number"
msgid "KDE Plasma %1 Beta %2"
msgstr "KDE Plasma %1 Beta %2"

#: desktopview.cpp:263
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1"
msgstr "KDE Plasma %1"

#: desktopview.cpp:269
#, kde-format
msgctxt "@info:usagetip"
msgid "Visit bugs.kde.org to report issues"
msgstr "Vizitu bugs.kde.org por raporti atentindaĵojn"

#: main.cpp:85
#, kde-format
msgid "Plasma Shell"
msgstr "Plasma-Ŝelo"

#: main.cpp:97
#, kde-format
msgid "Enable QML Javascript debugger"
msgstr "Ebligi QML-Javascript-erarsermilon"

#: main.cpp:100
#, kde-format
msgid "Do not restart plasma-shell automatically after a crash"
msgstr "Ne rekomenci plasma-shell aŭtomate post paneo"

#: main.cpp:103
#, kde-format
msgid "Force loading the given shell plugin"
msgstr "Devigi ŝargadon de la donita ŝela kromaĵo"

#: main.cpp:107
#, kde-format
msgid "Replace an existing instance"
msgstr "Anstataŭigi ekzistantan instancon"

#: main.cpp:110
#, kde-format
msgid ""
"Enables test mode and specifies the layout javascript file to set up the "
"testing environment"
msgstr ""
"Ebligas testan reĝimon kaj specifas la aranĝan Javaskripto-dosieron por "
"agordi la testan medion"

#: main.cpp:111
#, kde-format
msgid "file"
msgstr "dosiero"

#: main.cpp:115
#, kde-format
msgid "Lists the available options for user feedback"
msgstr "Listigas la disponeblajn opciojn por uzant-sugestoj"

#: main.cpp:197
#, kde-format
msgid "Plasma Failed To Start"
msgstr "Plasma Malsukcesis Starti"

#: main.cpp:198
#, kde-format
msgid ""
"Plasma is unable to start as it could not correctly use OpenGL 2 or software "
"fallback\n"
"Please check that your graphic drivers are set up correctly."
msgstr ""
"Plasma ne povas komenci, ĉar ĝi ne povis ĝuste uzi OpenGL 2 aŭ ties "
"programan retropaŝon\n"
"Bonvolu kontroli, ke viaj grafikaj peliloj estas ĝuste agorditaj."

#: osd.cpp:53
#, kde-format
msgctxt "OSD informing that the system is muted, keep short"
msgid "Audio Muted"
msgstr "Aŭdio silentigita"

#: osd.cpp:75
#, kde-format
msgctxt "OSD informing that the microphone is muted, keep short"
msgid "Microphone Muted"
msgstr "Mikrofono silentigita"

#: osd.cpp:91
#, kde-format
msgctxt "OSD informing that some media app is muted, eg. Amarok Muted"
msgid "%1 Muted"
msgstr "%1 silentigita"

#: osd.cpp:113
#, kde-format
msgctxt "touchpad was enabled, keep short"
msgid "Touchpad On"
msgstr "Tuŝplato Enŝaltita"

#: osd.cpp:115
#, kde-format
msgctxt "touchpad was disabled, keep short"
msgid "Touchpad Off"
msgstr "Tuŝplato Malŝaltita"

#: osd.cpp:122
#, kde-format
msgctxt "wireless lan was enabled, keep short"
msgid "Wifi On"
msgstr "Wifi Enŝaltita"

#: osd.cpp:124
#, kde-format
msgctxt "wireless lan was disabled, keep short"
msgid "Wifi Off"
msgstr "Wifi Malŝaltita"

#: osd.cpp:131
#, kde-format
msgctxt "Bluetooth was enabled, keep short"
msgid "Bluetooth On"
msgstr "Bluetooth Enŝaltita"

#: osd.cpp:133
#, kde-format
msgctxt "Bluetooth was disabled, keep short"
msgid "Bluetooth Off"
msgstr "Bluetooth Malŝaltita"

#: osd.cpp:140
#, kde-format
msgctxt "mobile internet was enabled, keep short"
msgid "Mobile Internet On"
msgstr "Poŝtelefono Interreto Enŝaltita"

#: osd.cpp:142
#, kde-format
msgctxt "mobile internet was disabled, keep short"
msgid "Mobile Internet Off"
msgstr "Poŝtelefono Interreto Malŝaltita"

#: osd.cpp:150
#, kde-format
msgctxt ""
"on screen keyboard was enabled because physical keyboard got unplugged, keep "
"short"
msgid "On-Screen Keyboard Activated"
msgstr "Ekrana Klavaro Aktivigita"

#: osd.cpp:153
#, kde-format
msgctxt ""
"on screen keyboard was disabled because physical keyboard was plugged in, "
"keep short"
msgid "On-Screen Keyboard Deactivated"
msgstr "Surekrana klavaro malaktivigita"

#: panelview.cpp:1029
#, kde-format
msgctxt "@action:inmenu"
msgid "Hide Panel Configuration"
msgstr "Kaŝi Panelagordon"

#: panelview.cpp:1031
#, kde-format
msgctxt "@action:inmenu"
msgid "Show Panel Configuration"
msgstr "Montri Panelagordon"

#: shellcontainmentconfig.cpp:105
#, kde-format
msgid "Internal Screen on %1"
msgstr "Interna Ekrano sur %1"

#: shellcontainmentconfig.cpp:109
#, kde-format
msgctxt "Screen manufacturer and model on connector"
msgid "%1 %2 on %3"
msgstr "%1 %2 sur %3"

#: shellcontainmentconfig.cpp:132
#, kde-format
msgid "Disconnected Screen %1"
msgstr "Malkonektita Ekrano %1"

#: shellcorona.cpp:206 shellcorona.cpp:208
#, kde-format
msgid "Show Desktop"
msgstr "Montri labortablon"

#: shellcorona.cpp:208
#, kde-format
msgid "Hide Desktop"
msgstr "Kaŝi Labortablo"

#: shellcorona.cpp:224
#, kde-format
msgid "Show Activity Switcher"
msgstr "Montri Aktivecan Ŝaltilon"

#: shellcorona.cpp:235
#, kde-format
msgid "Stop Current Activity"
msgstr "Ĉesigi Nunan Agadon"

#: shellcorona.cpp:243
#, kde-format
msgid "Switch to Previous Activity"
msgstr "Ŝanĝi al Antaŭa Agado"

#: shellcorona.cpp:251
#, kde-format
msgid "Switch to Next Activity"
msgstr "Ŝanĝi al Sekva Agado"

#: shellcorona.cpp:266
#, kde-format
msgid "Activate Task Manager Entry %1"
msgstr "Aktivigi Task-Administran Eniron %1"

#: shellcorona.cpp:293
#, kde-format
msgid "Manage Desktops And Panels..."
msgstr "Administri labortablojn kaj panelojn..."

#: shellcorona.cpp:315
#, kde-format
msgid "Move keyboard focus between panels"
msgstr "Movi klavaran fokuson inter paneloj"

#: shellcorona.cpp:2064
#, kde-format
msgid "Add Panel"
msgstr "Aldoni Panelon"

#: shellcorona.cpp:2106
#, kde-format
msgctxt "Creates an empty containment (%1 is the containment name)"
msgid "Empty %1"
msgstr "Malplena %1"

#: softwarerendernotifier.cpp:27
#, kde-format
msgid "Software Renderer In Use"
msgstr "Programara Bildigilo En Uzo"

#: softwarerendernotifier.cpp:28
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Software Renderer In Use"
msgstr "Programara Bildigilo En Uzo"

#: softwarerendernotifier.cpp:29
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Rendering may be degraded"
msgstr "Bildo povas esti malplibonigita"

#: softwarerendernotifier.cpp:39
#, kde-format
msgid "Never show again"
msgstr "Neniam montri denove"

#: userfeedback.cpp:36
#, kde-format
msgid "Panel Count"
msgstr "Panelo-kalkulo"

#: userfeedback.cpp:40
#, kde-format
msgid "Counts the panels"
msgstr "Nombras la panelojn"
