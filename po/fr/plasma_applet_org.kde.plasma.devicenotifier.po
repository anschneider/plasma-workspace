# translation of plasma_applet_notifier.po to Français
# Copyright (C) 2007 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sébastien Renard <Sebastien.Renard@digitalfox.org>, 2007, 2008, 2009.
# aminesay <aminesay@yahoo.fr>, 2007.
# Amine Say <aminesay@yahoo.fr>, 2008, 2009.
# Nicolas Ternisien <nicolas.ternisien@gmail.com>, 2008.
# Guillaume Pujol <guill.p@gmail.com>, 2010.
# Joëlle Cornavin <jcorn@free.fr>, 2012, 2013.
# Anne-Marie Mahfouf <annma@kde.org>, 2013.
# SPDX-FileCopyrightText: 2013, 2024 Xavier Besnard <xavier.besnard@kde.org>
# Thomas Vergnaud <thomas.vergnaud@gmx.fr>, 2014, 2016.
# Simon Depiets <sdepiets@gmail.com>, 2018, 2019.
# SPDX-FileCopyrightText: 2020, 2021, 2022, 2023 Xavier Besnard <xavier.besnard@kde.org>
#
# invite, 2010.
# amine say, 2010, 2011.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_notifier\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2024-01-23 10:54+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@kde.org>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 23.08.4\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: package/contents/ui/DeviceItem.qml:188
#, kde-format
msgctxt "@info:status Free disk space"
msgid "%1 free of %2"
msgstr "%1 libre sur %2"

#: package/contents/ui/DeviceItem.qml:192
#, kde-format
msgctxt ""
"Accessing is a less technical word for Mounting; translation should be short "
"and mean 'Currently mounting this device'"
msgid "Accessing…"
msgstr "Accès en cours..."

#: package/contents/ui/DeviceItem.qml:195
#, kde-format
msgctxt ""
"Removing is a less technical word for Unmounting; translation should be "
"short and mean 'Currently unmounting this device'"
msgid "Removing…"
msgstr "Suppression en cours…"

#: package/contents/ui/DeviceItem.qml:198
#, kde-format
msgid "Don't unplug yet! Files are still being transferred…"
msgstr ""
"Ne pas débrancher encore ! Les fichiers sont encore en cours de transfert..."

#: package/contents/ui/DeviceItem.qml:229
#, kde-format
msgid "Open in File Manager"
msgstr "Ouvrir dans le gestionnaire de fichiers"

#: package/contents/ui/DeviceItem.qml:232
#, kde-format
msgid "Mount and Open"
msgstr "Monter et ouvrir"

#: package/contents/ui/DeviceItem.qml:234
#, kde-format
msgid "Eject"
msgstr "Éjecter"

#: package/contents/ui/DeviceItem.qml:236
#, kde-format
msgid "Safely remove"
msgstr "Retirer en toute sécurité"

#: package/contents/ui/DeviceItem.qml:278
#, kde-format
msgid "Mount"
msgstr "Monter"

#: package/contents/ui/FullRepresentation.qml:43
#: package/contents/ui/main.qml:225
#, kde-format
msgid "Remove All"
msgstr "Tout supprimer"

#: package/contents/ui/FullRepresentation.qml:44
#, kde-format
msgid "Click to safely remove all devices"
msgstr "Cliquez pour retirer l'ensemble des périphériques en toute sécurité"

#: package/contents/ui/FullRepresentation.qml:186
#, kde-format
msgid "No removable devices attached"
msgstr "Aucun périphérique amovible branché"

#: package/contents/ui/FullRepresentation.qml:186
#, kde-format
msgid "No disks available"
msgstr "Aucun disque disponible"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "Most Recent Device"
msgstr "Le périphérique le plus récent"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "No Devices Available"
msgstr "Aucun périphérique disponible"

#: package/contents/ui/main.qml:207
#, kde-format
msgctxt "Open auto mounter kcm"
msgid "Configure Removable Devices…"
msgstr "Configurer les périphériques amovibles..."

#: package/contents/ui/main.qml:233
#, kde-format
msgid "Removable Devices"
msgstr "Périphériques amovibles"

#: package/contents/ui/main.qml:248
#, kde-format
msgid "Non Removable Devices"
msgstr "Périphériques non amovibles"

#: package/contents/ui/main.qml:263
#, kde-format
msgid "All Devices"
msgstr "Tous les périphériques"

#: package/contents/ui/main.qml:280
#, kde-format
msgid "Show popup when new device is plugged in"
msgstr "Afficher un menu contextuel lorsqu'un nouveau périphérique est branché"

#: plugin/ksolidnotify.cpp:190
#, kde-format
msgid "Device Status"
msgstr "État de périphérique"

#: plugin/ksolidnotify.cpp:190
#, kde-format
msgid "A device can now be safely removed"
msgstr "Ce périphérique peut désormais être retiré en toute sécurité."

#: plugin/ksolidnotify.cpp:191
#, kde-format
msgid "This device can now be safely removed."
msgstr "Ce périphérique peut désormais être retiré en toute sécurité."

#: plugin/ksolidnotify.cpp:198
#, kde-format
msgid "You are not authorized to mount this device."
msgstr "Vous n'êtes pas autorisé à monter ce périphérique."

#: plugin/ksolidnotify.cpp:201
#, kde-format
msgctxt "Remove is less technical for unmount"
msgid "You are not authorized to remove this device."
msgstr "Vous n'êtes pas autorisé à retirer ce périphérique."

#: plugin/ksolidnotify.cpp:204
#, kde-format
msgid "You are not authorized to eject this disc."
msgstr "Vous n'êtes pas autorisé à démonter ce disque."

#: plugin/ksolidnotify.cpp:211
#, kde-format
msgid "Could not mount this device as it is busy."
msgstr "Impossible de monter ce périphérique car il est occupé."

#: plugin/ksolidnotify.cpp:242
#, kde-format
msgid "One or more files on this device are open within an application."
msgstr ""
"Un ou plusieurs fichiers de ce périphérique sont ouverts par une application."

#: plugin/ksolidnotify.cpp:244
#, kde-format
msgid "One or more files on this device are opened in application \"%2\"."
msgid_plural ""
"One or more files on this device are opened in following applications: %2."
msgstr[0] ""
"Un ou plusieurs fichiers sur ce périphérique sont ouverts par l'application "
"« %2 »."
msgstr[1] ""
"Un ou plusieurs fichiers de ce périphérique sont ouverts dans les "
"applications suivantes : %2."

#: plugin/ksolidnotify.cpp:247
#, kde-format
msgctxt "separator in list of apps blocking device unmount"
msgid ", "
msgstr ", "

#: plugin/ksolidnotify.cpp:265
#, kde-format
msgid "Could not mount this device."
msgstr "Impossible de monter ce périphérique."

#: plugin/ksolidnotify.cpp:268
#, kde-format
msgctxt "Remove is less technical for unmount"
msgid "Could not remove this device."
msgstr "Impossible de retirer ce périphérique."

#: plugin/ksolidnotify.cpp:271
#, kde-format
msgid "Could not eject this disc."
msgstr "Impossible de démonter ce disque."

#~ msgid "Show:"
#~ msgstr "Afficher :"

#~ msgid "General"
#~ msgstr "Général"

#~ msgid ""
#~ "It is currently <b>not safe</b> to remove this device: applications may "
#~ "be accessing it. Click the eject button to safely remove this device."
#~ msgstr ""
#~ "Il est actuellement <b>déconseillé </b> de retirer ce périphérique : des "
#~ "applications peuvent y accéder en ce moment. Cliquez sur le bouton "
#~ "« Éjecter » pour retirer ce périphérique en toute sécurité."

#~ msgid "This device is currently accessible."
#~ msgstr "Ce périphérique est actuellement accessible."

#~ msgid ""
#~ "It is currently <b>not safe</b> to remove this device: applications may "
#~ "be accessing other volumes on this device. Click the eject button on "
#~ "these other volumes to safely remove this device."
#~ msgstr ""
#~ "Il est actuellement <b>déconseillé </b> de retirer ce périphérique : des "
#~ "applications peuvent accéder en ce moment à d'autres volumes sur ce "
#~ "périphérique. Cliquez sur le bouton « Éjecter » sur ces autres volumes "
#~ "pour retirer ce périphérique en toute sécurité."

#~ msgid "This device is not currently accessible."
#~ msgstr "Ce périphérique n'est pas accessible actuellement."

#~ msgid "1 action for this device"
#~ msgid_plural "%1 actions for this device"
#~ msgstr[0] "Une seule action pour ce périphérique"
#~ msgstr[1] "%1 actions pour ce périphérique"

#~ msgid "Available Devices"
#~ msgstr "Périphériques disponibles"
