# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2023, 2024 Alexander Yavorsky <kekcuha@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-02 00:39+0000\n"
"PO-Revision-Date: 2024-02-04 19:14+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 23.08.4\n"

#: package/contents/ui/BrightnessItem.qml:71
#, kde-format
msgctxt "Placeholder is brightness percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/main.qml:64
#, kde-format
msgid "Brightness and Color"
msgstr "Яркость и цвет"

#: package/contents/ui/main.qml:76
#, kde-format
msgid "Screen brightness at %1%"
msgstr "Яркость экрана: %1%"

#: package/contents/ui/main.qml:79
#, kde-format
msgid "Keyboard brightness at %1%"
msgstr "Яркость клавиатуры: %1%"

#: package/contents/ui/main.qml:83
#, kde-format
msgctxt "Status"
msgid "Night Light off"
msgstr "Ночная цветовая схема отключена"

#: package/contents/ui/main.qml:85
#, kde-format
msgctxt "Status; placeholder is a temperature"
msgid "Night Light at %1K"
msgstr "Цветовая температура ночной цветовой схемы: %1K"

#: package/contents/ui/main.qml:95
#, kde-format
msgid "Scroll to adjust screen brightness"
msgstr "Используйте колесо мыши для настройки яркости экрана"

#: package/contents/ui/main.qml:98
#, kde-format
msgid "Middle-click to toggle Night Light"
msgstr "Включить или отключить ночную цветовую схему: щелчок средней кнопкой"

#: package/contents/ui/main.qml:202
#, kde-format
msgctxt "@action:inmenu"
msgid "Configure Night Light…"
msgstr "Настроить ночную цветовую схему…"

#: package/contents/ui/NightColorItem.qml:75
#, kde-format
msgctxt "Night light status"
msgid "Off"
msgstr "Отключена"

#: package/contents/ui/NightColorItem.qml:78
#, kde-format
msgctxt "Night light status"
msgid "Unavailable"
msgstr "Недоступна"

#: package/contents/ui/NightColorItem.qml:81
#, kde-format
msgctxt "Night light status"
msgid "Not enabled"
msgstr "Не включена"

#: package/contents/ui/NightColorItem.qml:84
#, kde-format
msgctxt "Night light status"
msgid "Not running"
msgstr "Не запущена"

#: package/contents/ui/NightColorItem.qml:87
#, kde-format
msgctxt "Night light status"
msgid "On"
msgstr "Включена"

#: package/contents/ui/NightColorItem.qml:90
#, kde-format
msgctxt "Night light phase"
msgid "Morning Transition"
msgstr "Утренний переход"

#: package/contents/ui/NightColorItem.qml:92
#, kde-format
msgctxt "Night light phase"
msgid "Day"
msgstr "День"

#: package/contents/ui/NightColorItem.qml:94
#, kde-format
msgctxt "Night light phase"
msgid "Evening Transition"
msgstr "Вечерний переход"

#: package/contents/ui/NightColorItem.qml:96
#, kde-format
msgctxt "Night light phase"
msgid "Night"
msgstr "Ночь"

#: package/contents/ui/NightColorItem.qml:107
#, kde-format
msgctxt "Placeholder is screen color temperature"
msgid "%1K"
msgstr "%1K"

#: package/contents/ui/NightColorItem.qml:142
#, kde-format
msgid "Configure…"
msgstr "Настроить…"

#: package/contents/ui/NightColorItem.qml:142
#, kde-format
msgid "Enable and Configure…"
msgstr "Включить и настроить…"

#: package/contents/ui/NightColorItem.qml:168
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day complete by:"
msgstr "Переход к дневной схеме завершён в:"

#: package/contents/ui/NightColorItem.qml:170
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night scheduled for:"
msgstr "Переход к ночной схеме запланирован в:"

#: package/contents/ui/NightColorItem.qml:172
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night complete by:"
msgstr "Переход к ночной схеме завершён в:"

#: package/contents/ui/NightColorItem.qml:174
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day scheduled for:"
msgstr "Переход к дневной схеме запланирован в:"

#: package/contents/ui/PopupDialog.qml:66
#, kde-format
msgid "Display Brightness"
msgstr "Яркость экрана"

#: package/contents/ui/PopupDialog.qml:97
#, kde-format
msgid "Keyboard Brightness"
msgstr "Яркость клавиатуры"

#: package/contents/ui/PopupDialog.qml:128
#, kde-format
msgid "Night Light"
msgstr "Ночная цветовая схема"
