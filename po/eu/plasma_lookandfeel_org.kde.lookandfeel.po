# Translation for plasma_lookandfeel_org.kde.lookandfeel.po to Euskara/Basque (eu).
# Copyright (C) 2017-2024 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# SPDX-FileCopyrightText: 2023, 2024 KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-30 00:39+0000\n"
"PO-Revision-Date: 2024-01-30 19:23+0100\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.4\n"

#: ../sddm-theme/KeyboardButton.qml:19
#, kde-format
msgid "Keyboard Layout: %1"
msgstr "Teklatu-antolaera: %1"

#: ../sddm-theme/Login.qml:85
#, kde-format
msgid "Username"
msgstr "Erabiltzaile-izena"

#: ../sddm-theme/Login.qml:102 contents/lockscreen/MainBlock.qml:64
#, kde-format
msgid "Password"
msgstr "Pasahitza"

#: ../sddm-theme/Login.qml:144 ../sddm-theme/Login.qml:150
#, kde-format
msgid "Log In"
msgstr "Hasi saioa"

#: ../sddm-theme/Main.qml:200 contents/lockscreen/LockScreenUi.qml:276
#, kde-format
msgid "Caps Lock is on"
msgstr "Blok Maius aktibatua dago"

#: ../sddm-theme/Main.qml:212 ../sddm-theme/Main.qml:356
#: contents/logout/Logout.qml:200
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Egin lo"

#: ../sddm-theme/Main.qml:219 ../sddm-theme/Main.qml:363
#: contents/logout/Logout.qml:225 contents/logout/Logout.qml:238
#, kde-format
msgid "Restart"
msgstr "Berrabiarazi"

#: ../sddm-theme/Main.qml:226 ../sddm-theme/Main.qml:370
#: contents/logout/Logout.qml:251
#, kde-format
msgid "Shut Down"
msgstr "Itzali"

#: ../sddm-theme/Main.qml:233
#, kde-format
msgctxt "For switching to a username and password prompt"
msgid "Other…"
msgstr "Bestelakoa..."

#: ../sddm-theme/Main.qml:342
#, kde-format
msgid "Type in Username and Password"
msgstr "Tekleatu erabiltzaile-izena eta pasahitza"

#: ../sddm-theme/Main.qml:377
#, kde-format
msgid "List Users"
msgstr "Zerrendatu erabiltzaileak"

#: ../sddm-theme/Main.qml:452 contents/lockscreen/LockScreenUi.qml:363
#, kde-format
msgctxt "Button to show/hide virtual keyboard"
msgid "Virtual Keyboard"
msgstr "Alegiazko teklatua"

#: ../sddm-theme/Main.qml:526
#, kde-format
msgid "Login Failed"
msgstr "Saio-hastea huts egin du"

#: ../sddm-theme/SessionButton.qml:18
#, kde-format
msgid "Desktop Session: %1"
msgstr "Mahaigain saioa: %1"

#: contents/lockscreen/config.qml:19
#, kde-format
msgctxt "@title: group"
msgid "Clock:"
msgstr "Ordularia:"

#: contents/lockscreen/config.qml:22
#, kde-format
msgctxt "@option:check"
msgid "Keep visible when unlocking prompt disappears"
msgstr "Mantendu ikusgai giltzatzea askatzeko gonbita desagertzean"

#: contents/lockscreen/config.qml:33
#, kde-format
msgctxt "@title: group"
msgid "Media controls:"
msgstr "Euskarrien kontrolak:"

#: contents/lockscreen/config.qml:36
#, kde-format
msgctxt "@option:check"
msgid "Show under unlocking prompt"
msgstr "Erakutsi giltzapetik askatzeko gonbitaren azpian"

#: contents/lockscreen/LockScreenUi.qml:50
#, kde-format
msgid "Unlocking failed"
msgstr "Giltzapetik askatzeak huts egin du"

#: contents/lockscreen/LockScreenUi.qml:290
#, kde-format
msgid "Sleep"
msgstr "Egin lo"

#: contents/lockscreen/LockScreenUi.qml:296 contents/logout/Logout.qml:210
#, kde-format
msgid "Hibernate"
msgstr "Hibernatu"

#: contents/lockscreen/LockScreenUi.qml:302
#, kde-format
msgid "Switch User"
msgstr "Aldatu erabiltzailea"

#: contents/lockscreen/LockScreenUi.qml:387
#, kde-format
msgctxt "Button to change keyboard layout"
msgid "Switch layout"
msgstr "Aldatu antolaera"

#: contents/lockscreen/MainBlock.qml:107
#: contents/lockscreen/NoPasswordUnlock.qml:17
#, kde-format
msgid "Unlock"
msgstr "Giltzapetik askatu"

#: contents/lockscreen/MainBlock.qml:156
#, kde-format
msgid "(or scan your fingerprint on the reader)"
msgstr "(edo eskaneatu zure hatz-marka irakurgailuan)"

#: contents/lockscreen/MainBlock.qml:160
#, kde-format
msgid "(or scan your smartcard)"
msgstr "(edo eskaneatu zure txartel-adimenduna)"

#: contents/lockscreen/MediaControls.qml:59
#, kde-format
msgid "No title"
msgstr "Titulurik gabe"

#: contents/lockscreen/MediaControls.qml:60
#, kde-format
msgid "No media playing"
msgstr "Ez da euskarririk jotzen ari"

#: contents/lockscreen/MediaControls.qml:88
#, kde-format
msgid "Previous track"
msgstr "Aurreko aztarna"

#: contents/lockscreen/MediaControls.qml:100
#, kde-format
msgid "Play or Pause media"
msgstr "Jo edo eten euskarria"

#: contents/lockscreen/MediaControls.qml:110
#, kde-format
msgid "Next track"
msgstr "Hurrengo aztarna"

#: contents/logout/Logout.qml:151
#, kde-format
msgid "Installing software updates and restarting in 1 second"
msgid_plural "Installing software updates and restarting in %1 seconds"
msgstr[0] "Software eguneratzeak instalatu eta segundo 1 barru berrabiarazten"
msgstr[1] "Software eguneratzeak instalatu eta %1 segundo barru berrabiarazten"

#: contents/logout/Logout.qml:152
#, kde-format
msgid "Restarting in 1 second"
msgid_plural "Restarting in %1 seconds"
msgstr[0] "Berrabiatzeko segundo 1"
msgstr[1] "Berrabiatzeko %1 segundo"

#: contents/logout/Logout.qml:154
#, kde-format
msgid "Logging out in 1 second"
msgid_plural "Logging out in %1 seconds"
msgstr[0] "Saio-ixteko segundo 1"
msgstr[1] "Saio-ixteko %1 segundo"

#: contents/logout/Logout.qml:157
#, kde-format
msgid "Shutting down in 1 second"
msgid_plural "Shutting down in %1 seconds"
msgstr[0] "Itzaltzeko segundo 1"
msgstr[1] "Itzaltzeko %1 segundo"

#: contents/logout/Logout.qml:172
#, kde-format
msgid ""
"One other user is currently logged in. If the computer is shut down or "
"restarted, that user may lose work."
msgid_plural ""
"%1 other users are currently logged in. If the computer is shut down or "
"restarted, those users may lose work."
msgstr[0] ""
"Beste erabiltzaile batek du une honetan saioa irekita. Ordenagailua itzali "
"edo berrabiatzen bada, erabiltzaile horrek lana gal lezake."
msgstr[1] ""
"Beste %1 erabiltzailek dute une honetan saioa irekita. Ordenagailua itzali "
"edo berrabiatzen bada, erabiltzaile horiek lana gal lezakete."

#: contents/logout/Logout.qml:187
#, kde-format
msgid "When restarted, the computer will enter the firmware setup screen."
msgstr "Berrabiatzean, ordenagailua firmwarea ezartzeko pantailan sartuko da."

#: contents/logout/Logout.qml:201
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep Now"
msgstr "Egin lo orain"

#: contents/logout/Logout.qml:211
#, kde-format
msgid "Hibernate Now"
msgstr "Hibernatu orain"

#: contents/logout/Logout.qml:222
#, kde-format
msgctxt "@action:button Keep short"
msgid "Install Updates & Restart"
msgstr "Instalatu eguneratzeak eta berrabiarazi"

#: contents/logout/Logout.qml:223
#, kde-format
msgctxt "@action:button Keep short"
msgid "Install Updates & Restart Now"
msgstr "Instalatu eguneratzeak eta berrabiarazi orain"

#: contents/logout/Logout.qml:226 contents/logout/Logout.qml:239
#, kde-format
msgid "Restart Now"
msgstr "Berrabiarazi orain"

#: contents/logout/Logout.qml:252
#, kde-format
msgid "Shut Down Now"
msgstr "Itzali orain"

#: contents/logout/Logout.qml:262
#, kde-format
msgid "Log Out"
msgstr "Itxi saioa"

#: contents/logout/Logout.qml:263
#, kde-format
msgid "Log Out Now"
msgstr "Itxi saioa orain"

#: contents/logout/Logout.qml:273
#, kde-format
msgid "Cancel"
msgstr "Utzi"

#: contents/osd/OsdItem.qml:32
#, kde-format
msgctxt "Percentage value"
msgid "%1%"
msgstr "%%1"

#: contents/splash/Splash.qml:79
#, kde-format
msgctxt ""
"This is the first text the user sees while starting in the splash screen, "
"should be translated as something short, is a form that can be seen on a "
"product. Plasma is the project name so shouldn't be translated."
msgid "Plasma made by KDE"
msgstr "Plasma KDEk egin du"

#~ msgid "OK"
#~ msgstr "Ados"

#~ msgid "Switch to This Session"
#~ msgstr "Aldatu saioa honetara"

#~ msgid "Start New Session"
#~ msgstr "Hasi saio berria"

#~ msgid "Back"
#~ msgstr "Atzera"

#~ msgid "%1%"
#~ msgstr "%%1"

#~ msgid "Battery at %1%"
#~ msgstr "Bateria maila: %%1"

#~ msgctxt "Nobody logged in on that session"
#~ msgid "Unused"
#~ msgstr "Erabili gabe"

#~ msgctxt "User logged in on console number"
#~ msgid "TTY %1"
#~ msgstr "TTY %1"

#~ msgctxt "User logged in on console (X display number)"
#~ msgid "on TTY %1 (Display %2)"
#~ msgstr "TTY %1(e)an (%2 bistaratzailea)"

#~ msgctxt "Username (location)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

#~ msgid "100%"
#~ msgstr "% 100"

#~ msgid "Configure"
#~ msgstr "Konfiguratu"

#~ msgid "Configure KRunner Behavior"
#~ msgstr "Konfiguratu KRunner-ren jokabidea"

#~ msgid "Configure KRunner…"
#~ msgstr "Konfiguratu KRunner..."

#~ msgctxt "Textfield placeholder text, query specific KRunner"
#~ msgid "Search '%1'…"
#~ msgstr "Bilatu «%1»..."

#~ msgctxt "Textfield placeholder text"
#~ msgid "Search…"
#~ msgstr "Bilatu..."

#~ msgid "Show Usage Help"
#~ msgstr "Erakutsi erabileraren laguntza"

#~ msgid "Pin"
#~ msgstr "Iltzatu"

#~ msgid "Pin Search"
#~ msgstr "Iltzatu bilaketa"

#~ msgid "Keep Open"
#~ msgstr "Eduki irekita"

#~ msgid "Recent Queries"
#~ msgstr "Berriki erabilitako itaunak"

#~ msgid "Remove"
#~ msgstr "Kendu"

#~ msgid "in category recent queries"
#~ msgstr "berriki erabilitako itaunak kategorian"

#~ msgctxt "verb, to show something"
#~ msgid "Show:"
#~ msgstr "Erakutsi:"

#~ msgid "Configure Search Plugins"
#~ msgstr "Konfiguratu bilatzeko pluginak"

#~ msgctxt ""
#~ "This is the first text the user sees while starting in the splash screen, "
#~ "should be translated as something short, is a form that can be seen on a "
#~ "product. Plasma is the project name so shouldn't be translated."
#~ msgid "Plasma 25th Anniversary Edition by KDE"
#~ msgstr "Plasmaren 25. urteurreneko edizioa, KDEk egina"

#~ msgid "Close"
#~ msgstr "Itxi"

#~ msgctxt "verb, to show something"
#~ msgid "Show always"
#~ msgstr "Erakutsi beti"

#~ msgctxt "verb, to show something"
#~ msgid "Always show"
#~ msgstr "Beti erakutsi"

#~ msgid "Suspend"
#~ msgstr "Eseki"

#~ msgid "Reboot"
#~ msgstr "Berrabiarazi"

#~ msgid "Logout"
#~ msgstr "Itxi saioa"

#~ msgid "Different User"
#~ msgstr "Beste erabiltzaile bat"

#~ msgid "Log in as a different user"
#~ msgstr "Ireki saioa beste erabiltzaile baten gisa"

#, fuzzy
#~| msgid "Password"
#~ msgid "Password..."
#~ msgstr "Pasahitza"

#~ msgid "Login"
#~ msgstr "Hasi saioa"

#~ msgid "Switch"
#~ msgstr "Aldatu"
