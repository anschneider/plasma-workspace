# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2024 Toms Trasuns <toms.trasuns@posteo.net>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-17 00:39+0000\n"
"PO-Revision-Date: 2024-01-18 20:54+0200\n"
"Last-Translator: Toms Trasuns <toms.trasuns@posteo.net>\n"
"Language-Team: Latvian <kde-i18n-doc@kde.org>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"
"X-Generator: Lokalize 23.08.4\n"

#: ui/DayNightView.qml:116
#, kde-format
msgid ""
"Color temperature begins changing to night time at %1 and is fully changed "
"by %2"
msgstr ""
"Krāsu temperatūra sāk mainīties uz nakts laiku plkst. %1 un pilnībā nomainās "
"līdz plkst. %2"

#: ui/DayNightView.qml:119
#, kde-format
msgid ""
"Color temperature begins changing to day time at %1 and is fully changed by "
"%2"
msgstr ""
"Krāsu temperatūra sāk mainīties uz dienas laiku plkst. %1 un pilnībā "
"nomainās līdz plkst. %2"

#: ui/LocationsFixedView.qml:38
#, kde-format
msgctxt ""
"@label:chooser Tap should be translated to mean touching using a touchscreen"
msgid "Tap to choose your location on the map."
msgstr "Uzsitiet uz kartes, lai mainītu savu atrašanās vietu."

#: ui/LocationsFixedView.qml:39
#, kde-format
msgctxt ""
"@label:chooser Click should be translated to mean clicking using a mouse"
msgid "Click to choose your location on the map."
msgstr "Klikšķiniet uz kartes, lai mainītu savu atrašanās vietu."

#: ui/LocationsFixedView.qml:78 ui/LocationsFixedView.qml:103
#, kde-format
msgid "Zoom in"
msgstr "Pietuvināt"

#: ui/LocationsFixedView.qml:209
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Modified from <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>World location map</link> by "
"TUBS / Wikimedia Commons / <link url='https://creativecommons.org/licenses/"
"by-sa/3.0'>CC BY-SA 3.0</link>"
msgstr ""
"Modificēts no <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>Pasaules vietu kartes</link>, "
"ko izstrādāja TUBS / „Wikimedia Commons“ / <link url='https://"
"creativecommons.org/licenses/by-sa/3.0'>CC BY-SA 3.0</link>"

#: ui/LocationsFixedView.qml:222
#, kde-format
msgctxt "@label: textbox"
msgid "Latitude:"
msgstr "Platums:"

#: ui/LocationsFixedView.qml:249
#, kde-format
msgctxt "@label: textbox"
msgid "Longitude:"
msgstr "Garums:"

#: ui/main.qml:98
#, kde-format
msgid "The blue light filter makes the colors on the screen warmer."
msgstr "Zilās gaismas filtrs krāsas ekrānā padara siltākas."

#: ui/main.qml:154
#, kde-format
msgid "Switching times:"
msgstr "Izmaiņu laiki:"

#: ui/main.qml:157
#, kde-format
msgid "Always off"
msgstr "Vienmēr izslēgts"

#: ui/main.qml:158
#, kde-format
msgid "Sunset and sunrise at current location"
msgstr "Saullēkts un saulriets atrašanās vietā"

#: ui/main.qml:159
#, kde-format
msgid "Sunset and sunrise at manual location"
msgstr "Saullēkts un saulriets manuāli norādītā vietā"

#: ui/main.qml:160
#, kde-format
msgid "Custom times"
msgstr "Pielāgoti laiki"

#: ui/main.qml:161
#, kde-format
msgid "Always on night light"
msgstr "Nakts gaismu ieslēgt vienmēr"

#: ui/main.qml:184
#, kde-format
msgid "Day light temperature:"
msgstr "Dienas gaismas temperatūra:"

#: ui/main.qml:227 ui/main.qml:289
#, kde-format
msgctxt "Color temperature in Kelvin"
msgid "%1K"
msgstr "%1K"

#: ui/main.qml:232 ui/main.qml:294
#, kde-format
msgctxt "Night colour blue-ish; no blue light filter activated"
msgid "Cool (no filter)"
msgstr "Vēsa (bez filtra)"

#: ui/main.qml:239 ui/main.qml:301
#, kde-format
msgctxt "Night colour red-ish"
msgid "Warm"
msgstr "Silta"

#: ui/main.qml:246
#, kde-format
msgid "Night light temperature:"
msgstr "Nakts gaismas temperatūra:"

#: ui/main.qml:311
#, kde-format
msgctxt "@label The coordinates for the current location"
msgid "Current location:"
msgstr "Pašreizējā atrašanās vieta:"

#: ui/main.qml:317
#, kde-format
msgid "Latitude: %1°   Longitude: %2°"
msgstr "Garums: %1°   Platums: %2°"

#: ui/main.qml:339
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The device's location will be periodically updated using GPS (if available), "
"or by sending network information to <link url='https://location.services."
"mozilla.com'>Mozilla Location Service</link>."
msgstr ""
"Ierīces atrašanās vietu ierīce periodiski atjauninās, izmantojot GPS (ja "
"pieejams) vai nosūtot tīkla informāciju uz <link url='https://location."
"services.mozilla.com'>„Mozilla“ atrašanās vietas pakalpojumi</link>."

#: ui/main.qml:357
#, kde-format
msgid "Begin night light at:"
msgstr "Nakts gaismu sākt plkst.:"

#: ui/main.qml:370 ui/main.qml:393
#, kde-format
msgid "Input format: HH:MM"
msgstr "Ievades formāts: HH:MM"

#: ui/main.qml:380
#, kde-format
msgid "Begin day light at:"
msgstr "Dienas gaismu sākt plkst.:"

#: ui/main.qml:402
#, kde-format
msgid "Transition duration:"
msgstr "Pārejas ilgums:"

#: ui/main.qml:411
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minūte"
msgstr[1] "%1 minūtes"
msgstr[2] "%1 minūšu"

#: ui/main.qml:424
#, kde-format
msgid "Input minutes - min. 1, max. 600"
msgstr "Ievades minūtes — min. 1, maks. 600"

#: ui/main.qml:443
#, kde-format
msgid "Error: Transition time overlaps."
msgstr "Kļūda: pārejas laiki pārklājas."

#: ui/main.qml:466
#, kde-format
msgctxt "@info:placeholder"
msgid "Locating…"
msgstr "Nosaka vietu..."
