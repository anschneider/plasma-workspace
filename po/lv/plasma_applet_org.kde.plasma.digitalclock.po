# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Māris Nartišs <maris.kde@gmail.com>, 2023.
# SPDX-FileCopyrightText: 2024 Toms Trasuns <toms.trasuns@posteo.net>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-15 00:39+0000\n"
"PO-Revision-Date: 2024-01-18 22:44+0200\n"
"Last-Translator: Toms Trasuns <toms.trasuns@posteo.net>\n"
"Language-Team: Latvian <kde-i18n-doc@kde.org>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"
"X-Generator: Lokalize 23.08.4\n"

#: package/contents/config/config.qml:19
#, kde-format
msgid "Appearance"
msgstr "Izskats"

#: package/contents/config/config.qml:24
#, kde-format
msgid "Calendar"
msgstr "Kalendārs"

#: package/contents/config/config.qml:29
#: package/contents/ui/CalendarView.qml:432
#, kde-format
msgid "Time Zones"
msgstr "Laika joslas"

#: package/contents/ui/CalendarView.qml:144
#, kde-format
msgid "Events"
msgstr "Notikumi"

#: package/contents/ui/CalendarView.qml:153
#, kde-format
msgctxt "@action:button Add event"
msgid "Add…"
msgstr "Pievienot..."

#: package/contents/ui/CalendarView.qml:157
#, kde-format
msgctxt "@info:tooltip"
msgid "Add a new event"
msgstr "Pievienot jaunu notikumu"

#: package/contents/ui/CalendarView.qml:392
#, kde-format
msgid "No events for today"
msgstr "Šodienai nav notikumu"

#: package/contents/ui/CalendarView.qml:393
#, kde-format
msgid "No events for this day"
msgstr "Šai dienai nav notikumu"

#: package/contents/ui/CalendarView.qml:443
#, kde-format
msgid "Switch…"
msgstr "Pārslēgt..."

#: package/contents/ui/CalendarView.qml:444
#: package/contents/ui/CalendarView.qml:447
#, kde-format
msgid "Switch to another timezone"
msgstr "Pārslēgt uz citu laika joslu"

#: package/contents/ui/configAppearance.qml:48
#, kde-format
msgid "Information:"
msgstr "Informācija:"

#: package/contents/ui/configAppearance.qml:52
#, kde-format
msgid "Show date"
msgstr "Rādīt datumu"

#: package/contents/ui/configAppearance.qml:60
#, kde-format
msgid "Adaptive location"
msgstr "Adaptīvs novietojums"

#: package/contents/ui/configAppearance.qml:61
#, kde-format
msgid "Always beside time"
msgstr "Vienmēr blakus laikam"

#: package/contents/ui/configAppearance.qml:62
#, kde-format
msgid "Always below time"
msgstr "Vienmēr zem laika"

#: package/contents/ui/configAppearance.qml:70
#, kde-format
msgid "Show seconds:"
msgstr "Rādīt sekundes:"

#: package/contents/ui/configAppearance.qml:72
#, kde-format
msgctxt "@option:check"
msgid "Never"
msgstr "Nekad"

#: package/contents/ui/configAppearance.qml:73
#, kde-format
msgctxt "@option:check"
msgid "Only in the tooltip"
msgstr "Tikai paskaidrē"

#: package/contents/ui/configAppearance.qml:74
#: package/contents/ui/configAppearance.qml:94
#, kde-format
msgid "Always"
msgstr "Vienmēr"

#: package/contents/ui/configAppearance.qml:84
#, kde-format
msgid "Show time zone:"
msgstr "Rādīt laika joslu:"

#: package/contents/ui/configAppearance.qml:89
#, kde-format
msgid "Only when different from local time zone"
msgstr "Tikai, ja atšķiras no vietējās laika joslas"

#: package/contents/ui/configAppearance.qml:103
#, kde-format
msgid "Display time zone as:"
msgstr "Rādīt laika joslu kā:"

#: package/contents/ui/configAppearance.qml:108
#, kde-format
msgid "Code"
msgstr "Kodu"

#: package/contents/ui/configAppearance.qml:109
#, kde-format
msgid "City"
msgstr "Pilsētu"

#: package/contents/ui/configAppearance.qml:110
#, kde-format
msgid "Offset from UTC time"
msgstr "Atšķirību no UTC laika"

#: package/contents/ui/configAppearance.qml:122
#, kde-format
msgid "Time display:"
msgstr "Laika attēlošana:"

#: package/contents/ui/configAppearance.qml:127
#, kde-format
msgid "12-Hour"
msgstr "12 stundu"

#: package/contents/ui/configAppearance.qml:128
#: package/contents/ui/configCalendar.qml:51
#, kde-format
msgid "Use Region Defaults"
msgstr "Izmantot reģiona noklusējumus"

#: package/contents/ui/configAppearance.qml:129
#, kde-format
msgid "24-Hour"
msgstr "24 stundu"

#: package/contents/ui/configAppearance.qml:136
#, kde-format
msgid "Change Regional Settings…"
msgstr "Mainīt reģionālos iestatījumus..."

#: package/contents/ui/configAppearance.qml:147
#, kde-format
msgid "Date format:"
msgstr "Datuma formāts:"

#: package/contents/ui/configAppearance.qml:155
#, kde-format
msgid "Long Date"
msgstr "Garais datums"

#: package/contents/ui/configAppearance.qml:162
#, kde-format
msgid "Short Date"
msgstr "Īsais datums"

#: package/contents/ui/configAppearance.qml:169
#, kde-format
msgid "ISO Date"
msgstr "ISO datums"

#: package/contents/ui/configAppearance.qml:176
#, kde-format
msgctxt "custom date format"
msgid "Custom"
msgstr "Pielāgots"

#: package/contents/ui/configAppearance.qml:206
#, kde-format
msgid ""
"<a href=\"https://doc.qt.io/qt-6/qml-qtqml-qt.html#formatDateTime-method"
"\">Time Format Documentation</a>"
msgstr ""
"<a href=\"https://doc.qt.io/qt-6/qml-qtqml-qt.html#formatDateTime-method"
"\">Laika formatēšanas dokumentācija</a>"

#: package/contents/ui/configAppearance.qml:230
#, kde-format
msgctxt "@label:group"
msgid "Text display:"
msgstr "Teksta attēlošana:"

#: package/contents/ui/configAppearance.qml:232
#, kde-format
msgctxt "@option:radio"
msgid "Automatic"
msgstr "Automātiska"

#: package/contents/ui/configAppearance.qml:236
#, kde-format
msgctxt "@label"
msgid ""
"Text will follow the system font and expand to fill the available space."
msgstr "Teksts izmantos sistēmas fontu un aizpildīs visu pieejamo vietu."

#: package/contents/ui/configAppearance.qml:246
#, kde-format
msgctxt "@option:radio setting for manually configuring the font settings"
msgid "Manual"
msgstr "Manuāla"

#: package/contents/ui/configAppearance.qml:256
#, kde-format
msgctxt "@action:button"
msgid "Choose Style…"
msgstr "Izvēlēties stilu..."

#: package/contents/ui/configAppearance.qml:269
#, kde-format
msgctxt "@info %1 is the font size, %2 is the font family"
msgid "%1pt %2"
msgstr "%1 pt %2"

#: package/contents/ui/configAppearance.qml:277
#, kde-format
msgctxt "@title:window"
msgid "Choose a Font"
msgstr "Izvēlēties fontu"

#: package/contents/ui/configCalendar.qml:38
#, kde-format
msgid "General:"
msgstr "Vispārīgi:"

#: package/contents/ui/configCalendar.qml:39
#, kde-format
msgid "Show week numbers"
msgstr "Rādīt nedēļu numurus"

#: package/contents/ui/configCalendar.qml:44
#, kde-format
msgid "First day of week:"
msgstr "Pirmā nedēļas diena:"

#: package/contents/ui/configCalendar.qml:65
#, kde-format
msgid "Available Plugins:"
msgstr "Pieejamie spraudņi:"

#: package/contents/ui/configTimeZones.qml:42
#, kde-format
msgid ""
"Tip: if you travel frequently, add your home time zone to this list. It will "
"only appear when you change the systemwide time zone to something else."
msgstr ""
"Padoms: ja bieži ceļojat, šim sarakstam pievienojat savu mājas laika joslu. "
"Tā parādīsies tikai tad, ja mainīsies visas sistēmas laika josla uz kādu "
"citu."

#: package/contents/ui/configTimeZones.qml:79
#, kde-format
msgid "Clock is currently using this time zone"
msgstr "Šobrīd pulkstenis izmanto šo laika joslu"

#: package/contents/ui/configTimeZones.qml:81
#, kde-format
msgctxt ""
"@label This list item shows a time zone city name that is identical to the "
"local time zone's city, and will be hidden in the timezone display in the "
"plasmoid's popup"
msgid "Hidden while this is the local time zone's city"
msgstr "Paslēpta tikmēr, kamēr šī pilsēta atrodas vietējā laika joslā"

#: package/contents/ui/configTimeZones.qml:106
#, kde-format
msgid "Switch Systemwide Time Zone…"
msgstr "Mainīt sistēmas laika joslu..."

#: package/contents/ui/configTimeZones.qml:118
#, kde-format
msgid "Remove this time zone"
msgstr "Noņemt šo laika joslu"

#: package/contents/ui/configTimeZones.qml:128
#, kde-format
msgid "Systemwide Time Zone"
msgstr "Sistēmas laika josla"

#: package/contents/ui/configTimeZones.qml:128
#, kde-format
msgid "Additional Time Zones"
msgstr "Papildu laika joslas"

#: package/contents/ui/configTimeZones.qml:141
#, kde-format
msgid ""
"Add more time zones to display all of them in the applet's pop-up, or use "
"one of them for the clock itself"
msgstr ""
"Pievienojiet papildu laika joslas, ko rādīt pulksteņa paskaidrē un uz kurām "
"ātri pārslēgt pulksteni, kad vajadzīgs"

#: package/contents/ui/configTimeZones.qml:150
#, kde-format
msgid "Add Time Zones…"
msgstr "Pievienot laika joslas..."

#: package/contents/ui/configTimeZones.qml:160
#, kde-format
msgid "Switch displayed time zone by scrolling over clock applet"
msgstr "Pārslēgt laika joslas ritinot peles ritenīti virs pulksteņa"

#: package/contents/ui/configTimeZones.qml:167
#, kde-format
msgid ""
"Using this feature does not change the systemwide time zone. When you "
"travel, switch the systemwide time zone instead."
msgstr ""
"Šīs iespējas izmantošana nemainīs sistēmas laika joslu. Ja ceļojat, tad tā "
"vietā mainiet sistēmas laika joslu."

#: package/contents/ui/configTimeZones.qml:190
#, kde-format
msgid "Add More Timezones"
msgstr "Pievienot papildu laika joslas"

#: package/contents/ui/configTimeZones.qml:202
#, kde-format
msgid ""
"At least one time zone needs to be enabled. Your local timezone was enabled "
"automatically."
msgstr ""
"Jābūt ieslēgtai vismaz vienai laika joslai. Jūsu lokālā laika josla ir "
"ieslēgta automātiski."

#: package/contents/ui/configTimeZones.qml:238
#, kde-format
msgid "%1, %2 (%3)"
msgstr "%1, %2 (%3)"

#: package/contents/ui/configTimeZones.qml:240
#, kde-format
msgid "%1, %2"
msgstr "%1, %2"

#: package/contents/ui/main.qml:148
#, kde-format
msgid "Copy to Clipboard"
msgstr "Kopēt uz starpliktuvi"

#: package/contents/ui/main.qml:152
#, kde-format
msgid "Adjust Date and Time…"
msgstr "Mainīt datumu un laiku..."

#: package/contents/ui/main.qml:158
#, kde-format
msgid "Set Time Format…"
msgstr "Iestatīt laika formātu..."

#: package/contents/ui/Tooltip.qml:31
#, kde-format
msgctxt "@info:tooltip %1 is a localized long date"
msgid "Today is %1"
msgstr "Šodien ir %1"

#: package/contents/ui/Tooltip.qml:135
#, kde-format
msgctxt "@label %1 is a city or time zone name"
msgid "%1:"
msgstr "%1:"

#: plugin/clipboardmenu.cpp:110
#, kde-format
msgid "Other Calendars"
msgstr "Citi kalendāri"

#: plugin/clipboardmenu.cpp:118
#, kde-format
msgctxt "unix timestamp (seconds since 1.1.1970)"
msgid "%1 (UNIX Time)"
msgstr "%1 (UNIX laiks)"

#: plugin/clipboardmenu.cpp:121
#, kde-format
msgctxt "for astronomers (days and decimals since ~7000 years ago)"
msgid "%1 (Julian Date)"
msgstr "%1 (datums pēc Jūlija kalendāra)"

#: plugin/timezonemodel.cpp:147
#, kde-format
msgctxt "This means \"Local Timezone\""
msgid "Local"
msgstr "Vietējā"

#: plugin/timezonemodel.cpp:149
#, kde-format
msgid "System's local time zone"
msgstr "Sistēmas vietējā laika josla"
