# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# SPDX-FileCopyrightText: 2014, 2015, 2016, 2017, 2018, 2020, 2023 Vít Pelčák <vit@pelcak.org>
# Vit Pelcak <vit@pelcak.org>, 2021, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-13 00:39+0000\n"
"PO-Revision-Date: 2023-11-06 10:03+0100\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.08.2\n"
"X-Language: cs_CZ\n"
"X-Source-Language: en_US\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Vít Pelčák"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "vit@pelcak.org"

#: currentcontainmentactionsmodel.cpp:206
#, kde-format
msgid "Configure Mouse Actions Plugin"
msgstr "Nastavit modul činností myši"

#: desktopview.cpp:211
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Dev"
msgstr "KDE Plasma 6.0 Vývoj"

#: desktopview.cpp:214
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Alpha"
msgstr "KDE Plasma 6.0 Alpha"

#: desktopview.cpp:217
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Beta 1"
msgstr "KDE Plasma 6.0 Beta 1"

#: desktopview.cpp:220
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Beta 2"
msgstr "KDE Plasma 6.0 Beta 2"

#: desktopview.cpp:223
#, kde-format
msgctxt "@label %1 is the Plasma version, RC meaning Release Candidate"
msgid "KDE Plasma 6.0 RC1"
msgstr "KDE Plasma 6.0 RC1"

#: desktopview.cpp:226
#, kde-format
msgctxt "@label %1 is the Plasma version, RC meaning Release Candidate"
msgid "KDE Plasma 6.0 RC2"
msgstr "KDE Plasma 6.0 RC2"

#: desktopview.cpp:252
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Dev"
msgstr "KDE Plasma %1 Vývoj"

#: desktopview.cpp:256
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Beta"
msgstr "KDE Plasma %1 Beta"

#: desktopview.cpp:259
#, kde-format
msgctxt "@label %1 is the Plasma version, %2 is the beta release number"
msgid "KDE Plasma %1 Beta %2"
msgstr "KDE Plasma %1 Beta %2"

#: desktopview.cpp:263
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1"
msgstr "KDE Plasma %1"

#: desktopview.cpp:269
#, kde-format
msgctxt "@info:usagetip"
msgid "Visit bugs.kde.org to report issues"
msgstr "Pro hlášení chyb použijte http://bugs.kde.org"

#: main.cpp:85
#, kde-format
msgid "Plasma Shell"
msgstr "Shell Plasma"

#: main.cpp:97
#, kde-format
msgid "Enable QML Javascript debugger"
msgstr "Povolit ladicí nástroj QML JavaScript"

#: main.cpp:100
#, kde-format
msgid "Do not restart plasma-shell automatically after a crash"
msgstr "Nerestartovat plasma-shell po pádu automaticky"

#: main.cpp:103
#, kde-format
msgid "Force loading the given shell plugin"
msgstr "Vynutit načítání daného moduly shellu"

#: main.cpp:107
#, kde-format
msgid "Replace an existing instance"
msgstr "Nahradit existující instanci"

#: main.cpp:110
#, kde-format
msgid ""
"Enables test mode and specifies the layout javascript file to set up the "
"testing environment"
msgstr ""
"Povoluje testovací režim a určuje soubor javascript pro rozložení "
"testovacího prostředí"

#: main.cpp:111
#, kde-format
msgid "file"
msgstr "soubor"

#: main.cpp:115
#, kde-format
msgid "Lists the available options for user feedback"
msgstr "Seznam dostupných voleb pro uživatelskou zpětnou vazbu"

#: main.cpp:197
#, kde-format
msgid "Plasma Failed To Start"
msgstr "Start Plasma selhal"

#: main.cpp:198
#, kde-format
msgid ""
"Plasma is unable to start as it could not correctly use OpenGL 2 or software "
"fallback\n"
"Please check that your graphic drivers are set up correctly."
msgstr ""
"Plasma se nemůže spustit protože nemůže správně používat OpenGL 2 nebo "
"softwarovou zálohu.\n"
"Prosím, zkontrolujte, že máte správně nastaveny ovladače."

#: osd.cpp:53
#, kde-format
msgctxt "OSD informing that the system is muted, keep short"
msgid "Audio Muted"
msgstr "Zvuk ztlumen"

#: osd.cpp:75
#, kde-format
msgctxt "OSD informing that the microphone is muted, keep short"
msgid "Microphone Muted"
msgstr "Mikrofon ztlumen"

#: osd.cpp:91
#, kde-format
msgctxt "OSD informing that some media app is muted, eg. Amarok Muted"
msgid "%1 Muted"
msgstr "%1 ztlumený"

#: osd.cpp:113
#, kde-format
msgctxt "touchpad was enabled, keep short"
msgid "Touchpad On"
msgstr "Touchpad zapnut"

#: osd.cpp:115
#, kde-format
msgctxt "touchpad was disabled, keep short"
msgid "Touchpad Off"
msgstr "Touchpad vypnut"

#: osd.cpp:122
#, kde-format
msgctxt "wireless lan was enabled, keep short"
msgid "Wifi On"
msgstr "Wifi zapnuta"

#: osd.cpp:124
#, kde-format
msgctxt "wireless lan was disabled, keep short"
msgid "Wifi Off"
msgstr "Wifi vypnuta"

#: osd.cpp:131
#, kde-format
msgctxt "Bluetooth was enabled, keep short"
msgid "Bluetooth On"
msgstr "Bluetooth zapnut"

#: osd.cpp:133
#, kde-format
msgctxt "Bluetooth was disabled, keep short"
msgid "Bluetooth Off"
msgstr "Bluetooth vypnut"

#: osd.cpp:140
#, kde-format
msgctxt "mobile internet was enabled, keep short"
msgid "Mobile Internet On"
msgstr "Mobilní internet zapnut"

#: osd.cpp:142
#, kde-format
msgctxt "mobile internet was disabled, keep short"
msgid "Mobile Internet Off"
msgstr "Mobilní internet vypnut"

#: osd.cpp:150
#, kde-format
msgctxt ""
"on screen keyboard was enabled because physical keyboard got unplugged, keep "
"short"
msgid "On-Screen Keyboard Activated"
msgstr "Zapnuta klávesnice na obrazovce"

#: osd.cpp:153
#, kde-format
msgctxt ""
"on screen keyboard was disabled because physical keyboard was plugged in, "
"keep short"
msgid "On-Screen Keyboard Deactivated"
msgstr "Vypnuta klávesnice na obrazovce"

#: panelview.cpp:1029
#, kde-format
msgctxt "@action:inmenu"
msgid "Hide Panel Configuration"
msgstr ""

#: panelview.cpp:1031
#, kde-format
msgctxt "@action:inmenu"
msgid "Show Panel Configuration"
msgstr ""

#: shellcontainmentconfig.cpp:105
#, kde-format
msgid "Internal Screen on %1"
msgstr "Interní obrazovka na %1"

#: shellcontainmentconfig.cpp:109
#, kde-format
msgctxt "Screen manufacturer and model on connector"
msgid "%1 %2 on %3"
msgstr "%1 %2 na %3"

#: shellcontainmentconfig.cpp:132
#, kde-format
msgid "Disconnected Screen %1"
msgstr "Odpojená obrazovka %1"

#: shellcorona.cpp:206 shellcorona.cpp:208
#, kde-format
msgid "Show Desktop"
msgstr "Zobrazit plochu"

#: shellcorona.cpp:208
#, kde-format
msgid "Hide Desktop"
msgstr "Skrýt pracovní plochu"

#: shellcorona.cpp:224
#, kde-format
msgid "Show Activity Switcher"
msgstr "Zobrazit přepínač aktivit"

#: shellcorona.cpp:235
#, kde-format
msgid "Stop Current Activity"
msgstr "Zastavit současnou aktivitu"

#: shellcorona.cpp:243
#, kde-format
msgid "Switch to Previous Activity"
msgstr "Přepnout na předchozí aktivitu"

#: shellcorona.cpp:251
#, kde-format
msgid "Switch to Next Activity"
msgstr "Přepnout na následující aktivitu"

#: shellcorona.cpp:266
#, kde-format
msgid "Activate Task Manager Entry %1"
msgstr "Aktivovat záznam Správce úloh %1"

#: shellcorona.cpp:293
#, kde-format
msgid "Manage Desktops And Panels..."
msgstr "Spravovat plochy a panely..."

#: shellcorona.cpp:315
#, kde-format
msgid "Move keyboard focus between panels"
msgstr "Přesunovat zaměření klávesnice mezi panely"

#: shellcorona.cpp:2064
#, kde-format
msgid "Add Panel"
msgstr "Přidat panel"

#: shellcorona.cpp:2106
#, kde-format
msgctxt "Creates an empty containment (%1 is the containment name)"
msgid "Empty %1"
msgstr "Prázdný %1"

#: softwarerendernotifier.cpp:27
#, kde-format
msgid "Software Renderer In Use"
msgstr "Je použito softwarové vykreslování"

#: softwarerendernotifier.cpp:28
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Software Renderer In Use"
msgstr "Je použito softwarové vykreslování"

#: softwarerendernotifier.cpp:29
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Rendering may be degraded"
msgstr "Vykreslování může být zhoršeno"

#: softwarerendernotifier.cpp:39
#, kde-format
msgid "Never show again"
msgstr "Nikdy nezobrazovat"

#: userfeedback.cpp:36
#, kde-format
msgid "Panel Count"
msgstr "Počet panelů"

#: userfeedback.cpp:40
#, kde-format
msgid "Counts the panels"
msgstr "Počítá panely"
