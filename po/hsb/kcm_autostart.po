# translation of desktop_kdebase.po to Upper Sorbian
# Prof. Dr. Eduard Werner <e.werner@rz.uni-leipzig.de>, 2003.
# Eduard Werner <edi.werner@gmx.de>, 2005, 2008.
# Bianka Šwejdźic <hertn@gmx.de>, 2005, 2007.
msgid ""
msgstr ""
"Project-Id-Version: desktop_kdebase\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-18 00:39+0000\n"
"PO-Revision-Date: 2008-11-06 22:08+0100\n"
"Last-Translator: Eduard Werner <edi.werner@gmx.de>\n"
"Language-Team: en_US <kde-i18n-doc@lists.kde.org>\n"
"Language: hsb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n"
"%100==4 ? 2 : 3;\n"

#: autostartmodel.cpp:382
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr ""

#: autostartmodel.cpp:385
#, kde-format
msgid "\"%1\" does not exist."
msgstr ""

#: autostartmodel.cpp:388
#, kde-format
msgid "\"%1\" is not a file."
msgstr ""

#: autostartmodel.cpp:391
#, kde-format
msgid "\"%1\" is not readable."
msgstr ""

#: ui/entry.qml:52
#, fuzzy, kde-format
#| msgid "Name"
msgctxt ""
"@label The name of a Systemd unit for an app or script that will autostart"
msgid "Name:"
msgstr "Mjeno"

#: ui/entry.qml:58
#, fuzzy, kde-format
#| msgid "Status"
msgctxt ""
"@label The current status (e.g. active or inactive) of a Systemd unit for an "
"app or script that will autostart"
msgid "Status:"
msgstr "Status"

#: ui/entry.qml:64
#, kde-format
msgctxt ""
"@label A date and time follows this text, making a sentence like 'Last "
"activated on: August 7th 11 PM 2023'"
msgid "Last activated on:"
msgstr ""

#: ui/entry.qml:74
#, kde-format
msgctxt "@label Stop the Systemd unit for a running process"
msgid "Stop"
msgstr ""

#: ui/entry.qml:74
#, kde-format
msgctxt "@label Start the Systemd unit for a currently inactive process"
msgid "Start"
msgstr ""

#: ui/entry.qml:109
#, kde-format
msgid "Unable to load logs. Try refreshing."
msgstr ""

#: ui/entry.qml:113
#, kde-format
msgctxt "@action:button Refresh entry logs when it failed to load"
msgid "Refresh"
msgstr ""

#: ui/main.qml:33
#, kde-format
msgid "Make Executable"
msgstr ""

#: ui/main.qml:53
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr ""

#: ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr ""

#: ui/main.qml:66
#, kde-format
msgctxt "@action:button"
msgid "Add…"
msgstr ""

#: ui/main.qml:69
#, kde-format
msgctxt "@action:button"
msgid "Add Application…"
msgstr ""

#: ui/main.qml:74
#, fuzzy, kde-format
#| msgid "Run script..."
msgctxt "@action:button"
msgid "Add Login Script…"
msgstr "Skript wuwjesć ..."

#: ui/main.qml:79
#, fuzzy, kde-format
#| msgid "Run script..."
msgctxt "@action:button"
msgid "Add Logout Script…"
msgstr "Skript wuwjesć ..."

#: ui/main.qml:114
#, kde-format
msgid ""
"%1 has not been autostarted yet. Details will be available after the system "
"is restarted."
msgstr ""

#: ui/main.qml:137
#, kde-format
msgctxt ""
"@label Entry hasn't been autostarted because system hasn't been restarted"
msgid "Not autostarted yet"
msgstr ""

#: ui/main.qml:146
#, kde-format
msgctxt "@action:button"
msgid "See properties"
msgstr ""

#: ui/main.qml:157
#, fuzzy, kde-format
#| msgid "&Remove"
msgctxt "@action:button"
msgid "Remove entry"
msgstr "Wotst&ronić"

#: ui/main.qml:173
#, kde-format
msgid "Applications"
msgstr ""

#: ui/main.qml:176
#, kde-format
msgid "Login Scripts"
msgstr ""

#: ui/main.qml:179
#, kde-format
msgid "Pre-startup Scripts"
msgstr ""

#: ui/main.qml:182
#, kde-format
msgid "Logout Scripts"
msgstr ""

#: ui/main.qml:191
#, kde-format
msgid "No user-specified autostart items"
msgstr ""

#: ui/main.qml:192
#, kde-kuit-format
msgctxt "@info 'some' refers to autostart items"
msgid "Click the <interface>Add…</interface> button to add some"
msgstr ""

#: ui/main.qml:207
#, fuzzy, kde-format
#| msgid "Run script..."
msgid "Choose Login Script"
msgstr "Skript wuwjesć ..."

#: ui/main.qml:227
#, kde-format
msgid "Choose Logout Script"
msgstr ""

#: unit.cpp:26
#, kde-format
msgctxt "@label Entry is running right now"
msgid "Running"
msgstr ""

#: unit.cpp:27
#, kde-format
msgctxt "@label Entry is not running right now (exited without error)"
msgid "Not running"
msgstr ""

#: unit.cpp:28
#, kde-format
msgctxt "@label Entry is being started"
msgid "Starting"
msgstr ""

#: unit.cpp:29
#, kde-format
msgctxt "@label Entry is being stopped"
msgid "Stopping"
msgstr ""

#: unit.cpp:30
#, kde-format
msgctxt "@label Entry has failed (exited with an error)"
msgid "Failed"
msgstr ""

#: unit.cpp:83
#, kde-format
msgid "Error occurred when receiving reply of GetAll call %1"
msgstr ""

#: unit.cpp:155
#, kde-format
msgid "Failed to open journal"
msgstr ""

#, fuzzy
#~| msgid "Run"
#~ msgctxt ""
#~ "@title:column The name of the column that decides if the program is run "
#~ "on session startup, on session shutdown, etc"
#~ msgid "Run On"
#~ msgstr "Wuwjesć"

#, fuzzy
#~| msgctxt "@item Text character set"
#~| msgid "Disabled"
#~ msgctxt "The program will be run"
#~ msgid "Enabled"
#~ msgstr "njesteji k dispoziciji"

#, fuzzy
#~| msgctxt "@item Text character set"
#~| msgid "Disabled"
#~ msgctxt "The program won't be run"
#~ msgid "Disabled"
#~ msgstr "njesteji k dispoziciji"

#, fuzzy
#~ msgid "Desktop File"
#~ msgstr "Name=Dźěłowy powjerch"

#, fuzzy
#~| msgid "Scriptfile"
#~ msgid "Script File"
#~ msgstr "Skriptowa dataja"

#, fuzzy
#~| msgid "Add to ignores..."
#~ msgid "Add Program..."
#~ msgstr "K njewobkedźbowanym dodać..."
